from django.conf.urls import url
from .views import add_activity
from .views import index
#url for app
urlpatterns = [
    url(r'add_activity/$', add_activity, name='add_activity'),
    url(r'^$', index, name='index'),
]
