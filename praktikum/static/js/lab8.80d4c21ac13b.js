// FB initiation function
window.fbAsyncInit = () => {
    FB.init({
      appId      : '370440953407961',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
  
     // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
     // dan jalankanlah fungsi render di bawah, dengan parameter true jika
     // status login terkoneksi (connected)
     FB.getLoginStatus(function(response) {
       if (response.status === 'connected') {
         console.log(response);
         var loginFlag = true;
       } else if (response.status === 'not_authorized') {
         console.log('not connected to app');
         var loginFlag = false;
       } else {
         console.log('not logged in to fb');
         var loginFlag = false;
       }
  
       render(loginFlag);
     });
     // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
     // otomatis akan ditampilkan view sudah login
   };
  
  
  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login
  
      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        console.log(user);
        $('#lab8').html(
          '<div class="profile" style="margin:2%">' +
            //'<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture"style="margin:2%;" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data" style="margin:20px;">' +
              '<h1>' + user.name + '</h1>' +
              '<h3>' + user.about + '</h3>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<br><button class="logout" onclick="facebookLogout()">Logout</button><br>'
        );
  
        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h4>' + value.message + '</h4>' +
                  '<h5>' + value.story + '</h5>' +
                '<hr/ style="display: block;height: 1px;border: 0;border-top: 1px solid #ccc;margin-left: 5%;margin-right:5%;padding: 0; "></div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h4>' + value.message + '</h4>' +
                  '<hr/ style="display: block;height: 1px;border: 0;border-top: 1px solid #ccc;margin-left: 5%;margin-right:5%;padding: 0; "></div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h5>' + value.story + '</h5>' +
                  '<hr/ style="display: block;height: 1px;border: 0;border-top: 1px solid #ccc;margin-left: 5%;margin-right:5%;padding: 0; "></div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login with Facebook</button>');
    }
  };
  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
        console.log(response);
        location.reload();
      }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
  
  };
  const facebookLogout = () => {
     // TODO: Implement Method Ini
     // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
     // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
     FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);
        }
     });
  
   };
  
   // TODO: Lengkapi Method Ini
   // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
   // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
   // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
   // meneruskan response yang didapat ke fungsi callback tersebut
   // Apakah yang dimaksud dengan fungsi callback?
   const getUserData = (fun) => {
     FB.api('/me?fields=id,name,cover,picture,about,email,gender', 'GET', function(response){
          console.log(response);
          fun(response);
        });
  
   };
  
   const getUserFeed = (fun) => {
     // TODO: Implement Method Ini
     // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
     // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
     // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
     // tersebut
     FB.api(
       "/me/feed",
       function (response) {
         if (response && !response.error) {
           /* handle the result */
           fun(response);
         }
         else{
             alert("Maaf ada error saat ingin menampilkan post\nPesan error:\n" + response.error);
         }
       }
     );
  
   };
  
   const postFeed = (messages) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    console.log(messages);
    FB.api('/me/feed', 'POST', {message:messages});
    location.reload();
  
  
  };
  
  const postStatus = () => {
    const messages = $('#postInput').val();
    if (messages === ""){
      alert("Cant be empty");
    }
    else {
      postFeed(messages);
      $('#postInput').val('');
    }
  
  };
  