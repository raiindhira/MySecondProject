// Chat-box
var chatHead = document.getElementsByClassName('chat-head');
var chatBody = document.getElementsByClassName('chat-body');
var chatText = document.getElementById('isi-chat');
var chatLocInsert = document.getElementsByClassName('msg-insert');
var jadiPengirim = true;

$(chatHead).click(function(){
    $(chatBody).toggle();
});

$(chatText).keypress(function(e){
    if (e.keyCode === 13) {
        if (jadiPengirim){
            $(chatLocInsert).append('<p class="msg-send">'+chatText.value+'</p>')
            jadiPengirim=false;
        } else {
            $(chatLocInsert).append('<p class="msg-receive">'+chatText.value+'</p>')
            jadiPengirim=true;
        }
        chatText.value='';
    }
});
// END

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
